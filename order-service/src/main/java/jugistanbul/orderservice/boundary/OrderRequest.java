package jugistanbul.orderservice.boundary;

import jugistanbul.entity.EventObject;
import jugistanbul.orderservice.kafka.publish.KafkaPublisher;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author hakdogan (hakdogan@kodcu.com)
 * Created on 17.08.2020
 **/
@Path("order")
public class OrderRequest {
    
    public static CompletableFuture<EventObject> futureResponse;

    @Inject
    private KafkaPublisher kafkaPublisher;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response handleOrderRequest(final EventObject event) {
        event.setEvent("order");
        OrderRequest.futureResponse = new CompletableFuture<>();

        kafkaPublisher.publishRecordToKafka("ORDER_EVENT_TOPIC", event);
        
        try {
            // Block until the reply is received or timeout after 10 seconds
            EventObject reply = futureResponse.get(10, TimeUnit.SECONDS);
            OrderRequest.futureResponse = null;
            
            return Response.ok(reply).build();  // Return the reply as JSON
        } catch (Exception e) {
            return Response.status(Response.Status.GATEWAY_TIMEOUT).entity("Payment confirmation timeout").build();
        }
    }
}
