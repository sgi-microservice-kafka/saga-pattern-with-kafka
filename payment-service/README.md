# Payment Service

This service is responsible for consuming `ValidationEvent` from `Kafka` and publishing `BillingEvent` to it.